Welcome to the ChainUrRents app!

Author: Kristine Leetberg A92657

Important notice:
In the first version, it is important that each property has at least one booking associated with it. This aspect will be changed in the upcoming versions. 
In addition, since it was not specificied in the text, the authors have made an assumption that a booking cannot start on the same they as another booking ends. If needed, this can be changed in the upcoming versions.