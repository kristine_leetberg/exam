class PropertiesController < ApplicationController
  def index
    @start_date = Date.new(params[:booking]["start(1i)"].to_i, params[:booking]["start(2i)"].to_i, params[:booking]["start(3i)"].to_i)
    @end_date = Date.new(params[:booking]["end(1i)"].to_i, params[:booking]["end(2i)"].to_i, params[:booking]["end(3i)"].to_i)
    @properties = Property.joins(:bookings).where(:location => params[:property][:location]).where.not(bookings: {:start => @start_date..@end_date, :end => @start_date..@end_date})
  end

  def query
  end
end