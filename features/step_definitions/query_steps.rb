#require File.expand_path(File.dirname(__FILE__) + "/../../app/")

Given /the following properties exist/ do |properties_table|
  properties_table.hashes.each do |prop|
    Property.create prop
  end
end

Given /the following bookings exist/ do |bookings_table|
  bookings_table.hashes.each do |booking|
    Booking.create booking
  end
end