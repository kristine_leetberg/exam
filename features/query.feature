Feature: Query properties
  In order to see list of available properties
  As a user
  I want to be able to insert information to a form

  Background:
    Given the following properties exist:
      | name        | cost | location     | area | floor  | services          |
      | Property1   | 100  | Tartu        | 30   | 1      | Kitchen, Bathroom |
      | Property2   | 30   | Tartu        | 50   | 5      | Shower            |
      | Property3   | 70   | Tallinn      | 17   | 11     | Bath              |
      | Property4   | 25   | Tartu        | 8    | 4      | Fireplace         |

    And the following bookings exist:
      | property | start      | end        |
      | 4        | 02.02.2017 | 05.02.2017 |

  Scenario: Successful query
    Given I am on the query page
    When I fill in "City" with "Tartu"
    And I fill in "Start" with "04.02.2017"
    And I fill in "End" with "06.02.2017"
    And I press "Search"
    Then I should be on the properties page
    And I should see "Property1"
    And I should see "Property2"
    And I should not see "Property3"
    And I should not see "Property4"

  Scenario: Unsuccesful query
    Given I am on the query page
    When I fill in "City" with "Haapsalu"
    And I fill in "Start" with "04.02.2017"
    And I fill in "End" with "06.02.2017"
    And I press "Search"
    Then I should be on the properties page
    And I should not see "Property1"
    And I should not see "Property2"
    And I should not see "Property3"
    And I should not see "Property4"

