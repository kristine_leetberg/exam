class CreateBookings < ActiveRecord::Migration
  def change
    create_table :bookings do |t|
      t.belongs_to :property, index: true
      t.date 'start'
      t.date 'end'
    end
  end
end
