class CreateProperties < ActiveRecord::Migration
  def change
    create_table :properties do |t|
      t.string 'name'
      t.float 'cost'
      t.string 'location'
      t.float 'area'
      t.integer 'floor'
      t.text 'services'
    end
  end
end
