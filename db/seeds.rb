# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


@p1 = Property.create(name: 'Property1', cost: 100, location: 'Tartu', area: 30, floor: 1, services: 'Kitchen, Bathroom')
@p2 = Property.create(name: 'Property2', cost: 30, location: 'Tartu', area: 50, floor: 5, services: 'Kitchen, Bathroom')
@p3 = Property.create(name: 'Property3', cost: 70, location: 'Tallinn', area: 17, floor: 11, services: 'Kitchen, Bathroom')
@p4 = Property.create(name: 'Property4', cost: 25, location: 'Tartu', area: 8, floor: 4, services: 'Kitchen, Bathroom')

@b1 = @p1.bookings.create(start: Date.new(2017,01,24), end: Date.new(2017,01,25))
@b2 = @p4.bookings.create(start: Date.new(2017,02,03), end: Date.new(2017,02,05))
@b3 = @p2.bookings.create(start: Date.yesterday, end: Date.yesterday)
@b4 = @p3.bookings.create(start: Date.yesterday, end: Date.yesterday)

