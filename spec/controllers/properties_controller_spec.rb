require 'rails_helper'

RSpec.describe PropertiesController, :type => :controller do

  context "GET index" do

    it "renders the index template" do
      get :index
      expect(response).to render_template(:index)
    end
  end

  context "GET query" do
    let(:query) { FactoryGirl.build_stubbed(:query,
                                              location: 'Tartu', start: Faker::Date, end: Faker.Date) }


    it "assigns the requested query to @query" do
      get :query
      expect(assigns(:query)).to eq query
    end

    it "renders the query template" do
      get :query
      expect(response).to render_template(:query)
    end
  end


end

