require 'rails_helper'

RSpec.describe BookingsController, :type => :controller do

  context "GET index" do

    it "displays a list of properties" do
      get :index
      expect(response).to render_template(:index)
    end
  end


end

